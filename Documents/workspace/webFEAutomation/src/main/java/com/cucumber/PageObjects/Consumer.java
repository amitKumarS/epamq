package com.cucumber.PageObjects;

import static org.testng.Assert.fail;
import java.util.ArrayList;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import com.cucumber.base.ConfigFileReader;
import com.cucumber.listener.Reporter;

public class Consumer {
	WebDriver driver;
	ConfigFileReader configFileReader;

	private String DashboardTitle;
	private String actualTitle;
	private String blockURL, expectedTitle;
	private ArrayList<String> block, tab, ExpectedTitleallow;
	private String ExpectedTitleblock;

	public Consumer(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		configFileReader = new ConfigFileReader();

	}

	// Page Factory
	@FindBy(xpath = "//button[contains(text(),'OK')]")
	private WebElement cookies_OK;

	@FindBy(xpath = "//div[contains(@class,'right-menu')]//span[contains(text(),'My BT ')]")
	private WebElement my_BT;

	@FindBy(xpath = "//div[contains(@class,'right-menu')]//a[text()='Log in']")
	private WebElement log_in;

	@FindBy(xpath = "//div[contains(@class,'right-menu')]//div[contains(@class,'login-logout')]//a[text()='Log out']")
	private WebElement log_out;

	@FindBy(xpath = "//input[@name='USER']")
	private WebElement txtbx_username;



	public void navigationTo_URL(String site) throws InterruptedException {

		driver.get(configFileReader.getApplicationUrl(site));
		System.out.println("User is on Landing Page");
		Thread.sleep(5000);
	}

	

	

	public void navigateTo_BTParentalControl() throws InterruptedException {
		try {
			Thread.sleep(10000);
			DashboardTitle = driver.getTitle();
			System.out.println(DashboardTitle);

		} catch (Exception e) {
			e.printStackTrace();
			Reporter.addStepLog("----------Failed to navigate---------");
			fail();
		}
	}

	
	public void compareTitle(String Filter) throws InterruptedException {
		try {
			block = configFileReader.filteringTestData(Filter);
			for (int i = 0; i < block.size(); i++) {
				((JavascriptExecutor) driver).executeScript("window.open('about:blank','_blank')");
				tab = new ArrayList<String>(driver.getWindowHandles());
				System.out.println(tab.size());
				driver.switchTo().window(tab.get(1));
				blockURL = block.get(i);
				driver.get(blockURL);
				Thread.sleep(3000);
				actualTitle = driver.getTitle();
				if (Filter.equalsIgnoreCase("")) {
					ExpectedTitleallow = configFileReader.expectedTitles("");
					expectedTitle = ExpectedTitleallow.get(i);
					Assert.assertEquals(actualTitle, expectedTitle);
				} else if (Filter.equalsIgnoreCase("Blocked")) {
					ExpectedTitleblock = configFileReader.expectedTitleblock("");
					Assert.assertEquals(actualTitle, ExpectedTitleblock);
				} 
				}
				driver.close();
				driver.switchTo().window(tab.get(0));
			}
	
		 catch (Exception e) {
			e.printStackTrace();
			Reporter.addStepLog("---------Failed to Validate----------");
			fail();
		}
	}
}