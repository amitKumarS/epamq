package com.cucumber.runner;

import java.io.File;

import org.junit.AfterClass;
import org.junit.runner.RunWith;

import com.cucumber.listener.Reporter;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(plugin={"com.cucumber.listener.ExtentCucumberFormatter:Reports/cucumber-extent/report.html","pretty"}
				,monochrome=true
			 	)
public class TestRunner_t {
	private static String reportConfigPath="src/test/java/extent-config.xml";
	@AfterClass
	public static void reportSetup()
	{
		Reporter.loadXMLConfig(new File(reportConfigPath));
		 Reporter.setSystemInfo("User Name", System.getProperty("user.name"));
		    Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
		    Reporter.setSystemInfo("Machine", 	"Windows 8" + "32 Bit");
		    Reporter.setSystemInfo("Selenium", "3.7.0");
		    Reporter.setSystemInfo("Maven", "3.5.3");
		    Reporter.setSystemInfo("Java Version", "1.8.0_171");

		
		
	}

}
