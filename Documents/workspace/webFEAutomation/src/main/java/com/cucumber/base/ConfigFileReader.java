package com.cucumber.base;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import org.openqa.selenium.By;
import enums.DriverType;

public class ConfigFileReader {
	private static Properties prop;
	private static Properties testData;
	Properties properties;
	private final String dataFilePath = "Properties//TestData.properties";
	private final String propertyFilePath = "Properties//Config.properties";
	

	public ConfigFileReader() {
		BufferedReader reader;

		try {
			reader = new BufferedReader(new FileReader(propertyFilePath));
			prop = new Properties();
			try {
				prop.load(reader);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Config.properties not found at "
					+ System.getProperty("user.dir").replaceAll("\\\\", "/") + propertyFilePath);
		}
		try {
			testData = new Properties();
			FileInputStream ip = new FileInputStream("Properties//TestData.properties");
			testData.load(ip);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("TestData.properties not found at "
					+ System.getProperty("user.dir").replaceAll("\\\\", "/") + dataFilePath);
		}
	}
	public ConfigFileReader(String FilePath) {

		try {
			FileInputStream Locator = new FileInputStream(FilePath);
			properties = new Properties();
			properties.load(Locator);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	public long getImplicitlyWait() {
		String implicitlyWait = prop.getProperty("implicitlyWait");
		if (implicitlyWait != null)
			return Long.parseLong(implicitlyWait);
		else
			throw new RuntimeException("implicitlyWait not specified in the Config.properties file.");
	}

	public String getApplicationUrl(String site) {
		String URL;
		/// customer = prop.getProperty("customer");
		if (site.equalsIgnoreCase("DEMO_ONLINE_SHOP")) {
			URL = prop.getProperty("DEMO_ONLINE_SHOP");
			return URL;
		} else
			throw new RuntimeException("url not specified in the Config.properties file.");
	}

	public String getUsername(String user) {
		String username;
		if (user.equalsIgnoreCase("DEMO_ONLINE_SHOP")) {
			username = prop.getProperty("");
			return username;
		} else if (user.equalsIgnoreCase("")) {
			username = prop.getProperty("");
			return username;
		} else if (user.equalsIgnoreCase("")) {
			username = prop.getProperty("");
			return username;
		} else
			throw new RuntimeException("username not specified in the Config.properties file.");
	}

	public String getPassword(String user) {
		String password;
		if (user.equalsIgnoreCase("")) {
			password = prop.getProperty("");
			return password;
		} else if (user.equalsIgnoreCase("")) {
			password = prop.getProperty("");
			return password;
		} else if (user.equalsIgnoreCase("")) {
			password = prop.getProperty("");
			return password;
		} else
			throw new RuntimeException("password not specified in the Config.properties file.");
	}

	public DriverType getBrowser() {
		String browserName = prop.getProperty("browser");
		if (browserName == null || browserName.equals("chrome"))
			return DriverType.CHROME;
		else if (browserName.equalsIgnoreCase("firefox"))
			return DriverType.FIREFOX;
		else if (browserName.equals("iexplorer"))
			return DriverType.INTERNETEXPLORER;
		else
			throw new RuntimeException("Browser Name Key value in Config.properties is not matched : " + browserName);
	}

	
	public Boolean getBrowserWindowSize() {
		String windowSize = prop.getProperty("windowMaximize");
		if (windowSize != null)
			return Boolean.valueOf(windowSize);
		return true;
	}

	public String getData(String ElementName) throws Exception {
		// Read value using the logical name as Key
		String data = properties.getProperty(ElementName);
		return data;
	}

	public By getLocator(String ElementName) throws Exception {
		// Read value using the logical name as Key
		String locator = properties.getProperty(ElementName);
		// Split the value which contains locator type and locator value
		String locatorType = locator.split(":")[0];
		String locatorValue = locator.split(":")[1];
		// Return a instance of By class based on type of locator
		if (locatorType.toLowerCase().equals("id"))
			return By.id(locatorValue);
		else if (locatorType.toLowerCase().equals("name"))
			return By.name(locatorValue);
		else if ((locatorType.toLowerCase().equals("classname")) || (locatorType.toLowerCase().equals("class")))
			return By.className(locatorValue);
		else if ((locatorType.toLowerCase().equals("tagname")) || (locatorType.toLowerCase().equals("tag")))
			return By.className(locatorValue);
		else if ((locatorType.toLowerCase().equals("linktext")) || (locatorType.toLowerCase().equals("link")))
			return By.linkText(locatorValue);
		else if (locatorType.toLowerCase().equals("partiallinktext"))
			return By.partialLinkText(locatorValue);
		else if ((locatorType.toLowerCase().equals("cssselector")) || (locatorType.toLowerCase().equals("css")))
			return By.cssSelector(locatorValue);
		else if (locatorType.toLowerCase().equals("xpath"))
			return By.xpath(locatorValue);
		else
			throw new Exception("Locator type '" + locatorType + "' not defined!!");
	}
	public ArrayList<String> filteringTestData(String filterLevel) {
		// TODO Auto-generated method stub
		return null;
	}
	public ArrayList<String> expectedTitles(String string) {
		// TODO Auto-generated method stub
		return null;
	}
	public String expectedTitleblock(String string) {
		// TODO Auto-generated method stub
		return null;
	}



}
