@DemoOnlineShopFeature
Feature: DEMO_ONLINE_SHOP NAVIGATION SITE

Background:
	Given Open "DEMO_ONLINE_SHOP" site 
	And Navigate to BT parental Controls 
			
@ManageBlockSites
Scenario:  Verify that on adding sites to the allowed list, the resp sites should not be blocked & should be accessible
	Then Select "Moderate" filter level 
	Then Successfully "Added" entry to/from the "Blocked" list
	And Check that "Blocked" Sites are accessible or not
	
@SuspendBTParentalControls
Scenario: Verify that the filter should remain inactive  for 'Suspend BT Parental Controls' feature as per the time set up during the creation of filter
	Then Select "Moderate" filter level 
	And Validate that URLS for "Moderate" categories are blocked
	Then Set filter time for "Suspend BT Parental Controls" for the day
	And Validate that URLS for "Moderate" categories are unblocked
	
@AdditionalSecurityForHWtime
Scenario: Verify that the filter should remain active  for 'Additional security for homework time' feature as per the time set up during the creation of filter
	Then Select "Light" filter level
	And Validate that URLS for "Light" categories are blocked 
	Then Set filter time for "Additional security for homework time" for the day
	And Validate that URLS for "Social media,chat and games" categories are blocked

