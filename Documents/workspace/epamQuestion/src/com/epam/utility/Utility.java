package com.epam.utility;

import com.epam.designPattern.Bouquet;
import com.epam.designPattern.FlowerPriceConstant;

public class Utility implements Bouquet{

	static int totalCost=0;
	static FlowerPriceConstant FlowerConstant = FlowerPriceConstant.getInstance();


	@Override
	public int costCalc(int lilyCount, int roseCount, int jasmineCount) {
		
		
		totalCost=(lilyCount*FlowerConstant.LILY)+(roseCount*FlowerConstant.ROSE)+(jasmineCount*FlowerConstant.JASMINE);
		
		return totalCost;
		
	}

}
