package com.epam.designPattern;

public class FlowerPriceConstant {

	
	 public int LILY=3;
	 public int JASMINE=2;
	 public int ROSE=1;
	 
	 private static FlowerPriceConstant instance;
	
	 private FlowerPriceConstant(){
		 
	 }
	 public static FlowerPriceConstant getInstance() {
	     if(instance == null)
	         instance = new FlowerPriceConstant();
	     return instance;
	}
	
}
