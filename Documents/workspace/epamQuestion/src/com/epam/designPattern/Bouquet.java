/**
 * 
 */
package com.epam.designPattern;

/**
 * @author 
 *
 */
public interface Bouquet {
	
	public int costCalc(int lilyCount, int roseCount, int jasmineCount);

}
